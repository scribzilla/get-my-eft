# get-my-eft

Do you have a Form 1 that has been submitted with your electronic fingerprints? Would you like a copy of said fingerprints for future use? Follow this guide to pull a copy of your prints from an existing Form 1.

## Prerequisites

Before getting started you'll need a few things:

- ATF eForms account that has a Form 1 with fingerprints uploaded
- Tool for intercepting HTTP requests - ex: [HTTP Toolkit](https://httptoolkit.com/)
- Hex editor - ex: [Hex Fiend](https://hexfiend.com/)
- .EFT (Electronic Fingerprint Transmission) file viewer - ex: [NIST Viewer for Windows](https://www.netxsolutions.co.uk/nistviewer.aspx)

## Process

1. Open HTTP Toolkit and start a new intercept session with your browser.
2. Login to [eForms](https://eforms.atf.gov/login).
3. Open a Form 1 that has already had your electronic fingerprints uploaded.
4. Navigate to the Responsible Persons section and open the Line Item for your person.
5. Open HTTP Toolkit and pause collecting.
6. Look for a GET method call to the URL `https://eforms.atf.gov/api/socket.io/` that has a response body of ~3 MB. Copy the response body as hex.
7. Open Hex Fiend and paste the response body into a new file.
8. Search for the first occurrence of `.eft` to find the beginning file marker. The beginning of the file will start with `1.01:` soon after `.eft`.
9. Scroll down to the end of the file to find the ending file marker. Search for the first occurrence of that string and delete everything after it.
10. Save the file as `prints.eft`
11. Open NIST Viewer and open up your prints file!

## References

- [A Guide to .EFT File Creation and Submission - IDScan.net](https://idscan.net/a-guide-to-eft-file-creation-and-submission/)
- [What is an .EFT File? Electronic Fingerprint Transmission files explained. – National Gun Trusts](https://www.nationalguntrusts.com/blogs/nfa-gun-trust-atf-information-database-blog/what-is-an-eft-file)